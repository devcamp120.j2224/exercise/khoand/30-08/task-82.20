package com.devcamp.task82.service;

public interface INumberOfCustomerGroupByCountry {
   Integer getNumberOfCustomer();

   String getCountry();
}
